import React, { Component } from 'react'; 
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from 'react-router-dom';
import './App.css';
import Login from './components/admin/login';
import Reset from './components/admin/reset';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import TicketUI from './components/ticket/ticket';
import loginForm from './components/user/loginForm';
import registrationForm from './components/user/registrationForm';
import Cookies from 'js-cookie'
import home from './components/myBooking/mybooking';

// import 'bootstrap/dist/css/bootstrap.min.css';
class App extends Component {
  render(){
  return (
    <Router>
      <div className="App" >
      <nav className="navbar navbar-expand-lg navbar-light fixed-top" style={{backgroundColor:'blue',fontWeight:'bold',fontSize:'20px'}}>
        <div className="container" >
          <div className="collapse navbar-collapse"  id="navbarTogglerDemo02">
            <ul className="navbar-nav ml-auto">
            {
                !(Cookies.get('token') || Cookies.get('admin-token')) &&
            <li className="nav-item">
                <Link className="nav-link" to={"/admin"}>Admin</Link>
              </li>
            }
            {
                !(Cookies.get('token') || Cookies.get('admin-token')) &&
              <li className="nav-item">
                <Link className="nav-link" to={"/loginForm"}>Login</Link>
              </li>
            }
            {
                (window.location.pathname == '/mybooking') &&
              <li className="nav-item">
                <Link className="nav-link" onClick = { () => {window.location.href= '/ticket'}}>Home</Link>
              </li>
            }
              {
                (window.location.pathname == '/ticket') &&
              <li className="nav-item">
                <Link className="nav-link" onClick = { () => {window.location.href= '/mybooking'}}>My Bookings</Link>
              </li>
              }
              {
                (Cookies.get('token') || Cookies.get('admin-token')) &&
                <li className="nav-item">
                <Link className="nav-link" onClick = { () => { 
                  Cookies.remove('admin-token');
                  Cookies.remove('token');
                  Cookies.remove('userid');
                  Cookies.remove('email');
                  Cookies.remove('phone');
                  Cookies.remove('name');
                  window.location.href= '/loginForm';
                  // alert("Logout Success")
                } }>Log out</Link>
              </li>
              }
            </ul>
          </div>
        </div>
      </nav>
    </div>
    <div className="auth-wrapper">
        <div className="auth-inner">
          <Switch>
          {/* <Route path='/' exact={true} component={loginForm} /> */}
          
           <Route path="/admin" component={Login} />
           <Route path='/reset' component={Reset}></Route> 
           <Route path='/ticket' component={TicketUI}></Route>
            <Route path="/loginForm" component={loginForm} />
            <Route path="/registrationForm" component={registrationForm} />
            <Route path="/mybooking" component={home} />
            <Redirect from="/" to="/loginForm" />
          </Switch>
        </div>
      </div>  
    

    </Router>
  );
}
}

export default App;
