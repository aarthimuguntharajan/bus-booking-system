import React from 'react';
import Cookies from 'js-cookie'
import SweetAlert from 'react-bootstrap-sweetalert';
import './style.css'
import Dropdown from 'react-bootstrap/Dropdown'
import {Button,Row,Col, Container} from 'reactstrap';



class TicketUI extends React.Component {

    constructor (props){
        super(props);
        if (!Cookies.get('token')){
            window.location.href = '/loginForm';
        }
        this.state = {
        busData: [],
        openData: [],
        closedData: [],
        tickets: [],
        reserved: [],
        alert: null
     };
     this.getOpenTickets = this.getOpenTickets.bind(this)
     this.getClosedTickets = this.getClosedTickets.bind(this)
     this.bookTickets = this.bookTickets.bind(this)
     this.getTickets = this.getTickets.bind(this)
     this.getComponent = this.getComponent.bind(this)
     }
     componentWillMount(){
        
        fetch('http://localhost:5555/show',{
            method: 'GET',
            headers:{
                'contene-type': "application/json",
                // "x-access-token": Cookies.get('token') 
            }
        }).then(response=> response.json())
        .then(dataObj => {
            if (dataObj.auth === true){
                const bus = dataObj.data.Buses
                this.setState({ 
                    busData : (bus)
                })
            }
        })
    }
    
    

    
    async getOpenTickets(busId) { 

        var self = this;

        await fetch ("http://localhost:5555/ticket/open/"+busId, {
            method: "GET",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              "x-access-token": Cookies.get('token')   
            }
        }).then(response => 
            response.json())
            .then(jsonObj => {
                if (jsonObj.auth === true){
                    if(jsonObj.data){
                        self.setState({ 
                            openData : (jsonObj.data.tickets)

                        })
                    }
                    else{
                        self.setState({ 
                            openData : []
                        })
                        alert("No Open Ticket")
                    }
                    
                }
                
            })
    }

    

    async getClosedTickets(busId){ 
        var self = this;
        await fetch ("http://localhost:5555/ticket/closed/"+busId, {
            method: "GET",
            headers: {
                'content-type':'application/json',
                "x-access-token": Cookies.get('token')
            }
        }).then(response => 
            response.json())
            .then(jsonObj => {
                if (jsonObj.auth === true){
                    if(jsonObj.data){
                        self.setState({ 
                            closedData : (jsonObj.data.tickets)
                        })
                    }
                    else{
                        self.setState({ 
                            closedData : []
                        })
                        alert("No Closed Ticket")
                    }
                }
                
            })
    }

    async getTickets(busId){
        await this.getOpenTickets(busId);
        await this.getClosedTickets(busId);
        if (this.state.openData || this.state.closedData){
            // console.log('open',this.state.openData,'closed',this.state.closedData);
            let result = [...this.state.openData, ...this.state.closedData];
            // const tickets = result.sort((a,b) => a-b)
            await this.setState({
                tickets : result.sort((a,b) => a-b)
            })
        }
    }
    hideAlert() {
        console.log('Hiding alert...');
        this.setState({
          alert: null
        });
      }

    bookTickets(busId,ticketId){
        const bookingInfo = {
            "busId": busId,
            "ticketId": ticketId,
            "userId": Cookies.get('userid')
        }
        fetch ("http://localhost:5555/booking/add ", {
            method: "POST",
            headers: {
                'content-type':'application/json',
                "x-access-token": Cookies.get('token')
            },
            body: JSON.stringify(bookingInfo)
        }).then(response => 
            response.json())
            .then(jsonObj => {
                    // alert(jsonObj.msg)
                    const getAlert = () => (
                        <SweetAlert 
                          success 
                          title= "Message"
                          onConfirm={() => this.hideAlert()}
                        >
                          {jsonObj.msg}
                        </SweetAlert>
                      );
                  
                      this.setState({
                        alert: getAlert()
                      })
                }
                ).then(() => 
                this.getTickets(busId))}

    // buttonSelection(data){
    //     if (data.status === 'OPEN'){
    //         return <button type="button" class="openButton btn btn-info btn-sm" onClick = {() => this.bookTickets(data.busId,data.ticketId)}>Book Ticket</button>
    //         //return <button>Book Ticket</button>
    //     }

    // }
    getComponent(event) {
        const name = event.target.className
        if (name === 'seat active'){
            event.target.className = 'seat'
        }
        else{
            event.target.className = name+' active'
            console.log('li item clicked!',event.target.className);
        }
    }
    
    render() {
    
      return (
        <div style={{marginTop:'30px'}}>
                <div class="container">
                    <h1 class="text-center text-black pt-5">Home Page</h1>
                    <div class="form-group" style={{paddingLeft:'450px'}}>
                        <Dropdown>
                            <Dropdown.Toggle variant="success" id="dropdown-basic">
                                Select Bus for Booking
                            </Dropdown.Toggle>

                            <Dropdown.Menu>
                            {this.state.busData.map((dynamicData, key) => {
                                return (
                                <Dropdown.Item onClick={()=>{this.getTickets(dynamicData.busId)}}>{dynamicData.busName +'-->'+ dynamicData.busFromTo}</Dropdown.Item>
                                )
                                })
                            }
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>
                </div>
                {/* <Results /> */}
                <div className="App"  >
                <h1>Ticket Details</h1>
                <div class="container theatre" style={{marginTop:'45px'}}>
                <Row xs='5'>
                    {this.state.tickets.map((dynamicData, index) => {
                        //  console.log(this.state.tickets);
                        var a = this.state.busData.filter(function(el) {
                            return el.busId === dynamicData.busId;
                          });
                        //   parseInt(index/10)+1);
                        return (
                            <Col>
                                <div class="cinema-seats left">
                                    <div class="cinema-row row-1">
                                        <div class="seat" onClick={()=>{this.bookTickets(dynamicData.busId,dynamicData.ticketId)}} style={(dynamicData.status === 'CLOSED')?{ pointerEvents:'none',opacity:0.5,background: '#CCC'}:{}}>
                                            <div>
                                                <h2>{dynamicData.seatNo}</h2>
                                                <p>{dynamicData.status==='CLOSED'?'Already Booked':''}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                    )})}
                </Row>
                </div>
                {/* <table class="table table-bordered">
                  <thead>
                      <tr>
                          <th>Ticket ID</th>
                          <th>Bus ID</th>
                          <th>Seat No</th>
                          <th>Status</th>
                          
                      </tr>
                  </thead>
                  <tbody>
                      {this.state.tickets.map((dynamicData, key) => {
                          return (
                          <tr key={key}>
                              <td>{dynamicData.ticketId}</td>
                              <td>{dynamicData.busId}</td>
                              <td>{dynamicData.seatNo}</td>
                              <td>{dynamicData.status}</td>
                              { this.buttonSelection(dynamicData)}
                              
                          </tr>
                      )
                      })}
                      
                  </tbody>
                </table>*/}
              </div>
              {this.state.alert}
            </div>
        
        )
          } 
  
}

  export default TicketUI