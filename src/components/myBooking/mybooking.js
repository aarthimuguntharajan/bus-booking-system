import React, { Component } from 'react'
import Cookies from 'js-cookie'
import Card from "react-bootstrap/Card";
import {Row,Col} from 'reactstrap';
import SweetAlert from 'react-bootstrap-sweetalert';


class home extends Component {
    constructor (props){
        super(props);
        if (!Cookies.get('token')){
            window.location.href = '/loginForm';
        }
        this.state = {
        data: [],
        alert: null
     };
     this.myBookings()
     this.cancelBooking = this.cancelBooking.bind(this)
     this.myBookings = this.myBookings.bind(this)
    }

    async myBookings(){
        await fetch('http://localhost:5555/booking/mybookings/'+Cookies.get('userid'),{
            method: 'GET',
            headers:{
                'contene-type': "application/json",
                "x-access-token": Cookies.get('token') 
            }
        }).then(response=> response.json())
        .then(dataObj => {
            if (dataObj.auth == true && !dataObj.msg){
                const bus = dataObj.data.bookings
                this.setState({ 
                    data : (bus)

                })
            }
            else{
                this.setState({ 
                    data : []
                })
            }
        })
    }

    hideAlert() {
        console.log('Hiding alert...');
        this.setState({
          alert: null
        });
      }

    async cancelBooking(bookingId){
        console.log("enter");
        await fetch('http://localhost:5555/booking/cancel/'+bookingId,{
            method: 'PUT',
            headers:{
                'contene-type': "application/json",
                "x-access-token": Cookies.get('token') 
            }
        }).then(response=> response.json())
        .then(dataObj => {
            if (dataObj.auth == true){
                const getAlert = () => (
                    <SweetAlert 
                      success 
                      title= "Message"
                      onConfirm={() => this.hideAlert()}
                    >
                      {dataObj.msg}
                    </SweetAlert>
                  );
              
                  this.setState({
                    alert: getAlert()
                  })
            }
        }).then(async ()=> await this.myBookings())

    }

    render() {
        return (
            <div style={{marginTop:'30px'}}>
                <div class="container">
                    <h1 class="text-center text-black pt-5">My Bookings</h1>
                    <div class="form-group" >
                        <div  class="container">
                            <Row md='2'>
                                {
                                    this.state.data.map((dynamicData, Key) => {
                                        return (
                                            <Col>
                                                <Card style={{ width: '28rem',flex: 1}}>
                                                    <Card.Body>
                                                    <Card.Title>Booking {Key+1}</Card.Title>
                                                    {/* <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle> */}
                                                    <Card.Text>
                                                        <p>Bus Name:{dynamicData.busName}</p>
                                                        <p>Seat No: {dynamicData.seatNo}</p>
                                                        <p>User Name: {Cookies.get('name')}</p>
                                                        <p>User Email: {Cookies.get('email')}</p>
                                                        <p>User Phone: {Cookies.get('phone')}</p>
                                                    </Card.Text>
                                                    <Card.Link onClick={()=> { this.cancelBooking(dynamicData.bookingId);this.forceUpdate();}}>Cancel Booking</Card.Link>
                                                    </Card.Body>
                                                </Card>
                                            </Col>
                                        )
                                    })
                                }
                            </Row>
                        </div>
                    </div>
                </div>
                {this.state.alert}
            </div>
        )
    }
}

export default home