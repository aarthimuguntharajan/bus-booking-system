import React, { Component } from "react";
import {  Link } from 'react-router-dom';
import { Button, Col, Row, Container } from 'react-bootstrap';
import {FormGroup,Input,Card,CardBody} from 'reactstrap';
import Cookies from 'js-cookie'
const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


export default class loginForm extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            password:'',
            email:'',
            redirect: false
             
    };
    if (Cookies.get('token')){
        window.location.href= '/ticket';
    }
    this.handleSubmit = this.handleSubmit.bind(this)
   };

   
     handleSubmit(e){
        e.preventDefault();
        console.log(this.state);
        const data = {
            email: this.state.email,
            password : this.state.password
        };

        if(this.state.email === "" || this.state.password === "" )

        {
            alert("All Fields are mandatory");

        }
        if(re.test(this.state.email) === false){
            alert("Email is Invalid");
        }
        if(this.state.password.length < 8)
        {
            alert("Password must atleast 8 characters");   
        }
        else{
             fetch('http://localhost:5555/auth/login', { 
                        method : 'POST' , 
                        headers: {
                            'content-type':'application/json'
                        },        
                        body : JSON.stringify(data)
                        })
                        .then(res => res.json())
                        .then(data => {
                            console.log(data);
                            if(data.msg === "Bad payload"){
                                alert("shouldnt be empty");
                            }
                            else if(data.msg !== "Bad payload" ){
                                Cookies.set("token", data.token);
                                Cookies.set("email", data.data.email);
                                Cookies.set("userid", data.data.id);
                                Cookies.set("phone", data.data.phone);
                                Cookies.set("name", data.data.name);
                                // alert("Login success");
                                window.location.href = '/ticket'
                            }
                        })                       
            }               
        
    };                                              


    render() {

        return (
            <div style={{marginTop:'30px'}}>

      
            <Container>
            
            <Row>
                <Col xs={12}><h3 className="text-center" style={{marginTop:'4%'}} >User Login </h3>
                </Col>
            </Row>

            <Row style={{marginTop:'4%'}}>
            <Col lg="3" md="3" sm="12" xs="12">
                </Col>
            <Col lg="6" md="6" sm="12" xs="12">
            
            <Card>
            <CardBody style={{backgroundColor:'#fff'}}>
                <form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <p style={{textAlign:'left'}} >User Email</p>
                        <Input
                            type="email"
                            name="email"
                            placeholder="User Email"
                            value = {this.state.email}
                            onChange={(event) => this.setState({email: event.target.value})}                            
                        />
                    </FormGroup>
                    <FormGroup>
                        <p style={{textAlign:'left'}} >Password</p>
                        <Input
                            type="password"
                            name="Password"
                            placeholder=" Enter Password"
                            value = {this.state.password}
                            onChange={(event) => this.setState({password: event.target.value})}
                            />
                    </FormGroup>
                    <Link className="nav-link" to={"/registrationForm"}>New User? Register here</Link>
                    <Button  variant="primary" type="submit" onClick={this.handleSubmit} active>  
                            Submit
                    </Button>
                </form>    
                </CardBody>
                </Card>
                </Col>
                </Row>
                </Container>
            
         </div>
        );
    }
}