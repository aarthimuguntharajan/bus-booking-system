import React, { Component } from "react";
import { Button, Col, Row, Container, Form } from 'react-bootstrap';
import {FormGroup,Input,Card,CardBody} from 'reactstrap';
const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default class registrationForm extends Component{
    
    constructor(props) {
        super(props);
        this.state = { 
            password: '',
            email: '',
            phone: '',
            name: ''         
    };

    this.handleSubmit = this.handleSubmit.bind(this)
    //this.handleChange = this.handleChange.bind(this)
   };
   
    async handleSubmit(e){
        e.preventDefault();
        
        const data = {
            email: this.state.email,
            password : this.state.password,
            name : this.state.name,
            phone : this.state.phone
        }
        //console.log(this.state);
        
        if(this.state.email === "" || this.state.password === "" || this.state.name === "" || this.state.phone === ""){
            alert("All Fields are Mandatory");
        }
        if(re.test(this.state.email) === false ){
            alert("Email is not valid");
        }
        if(this.state.password.length < 8)
        {
            alert("Password must atleast 8 characters");   
        }
        if(this.state.phone.length < 10){
            alert("Phone Number must be atleast 10 character");
        }
        else {
            await fetch('http://localhost:5555/auth/register', { 
                    method : 'POST' , 
                    headers: {
                        'content-type':'application/json'
                    },        
                    body : JSON.stringify(data)
                    })
                    .then(res => res.json())
                    .then((data) => {
                        if(data.auth === true){
                                    localStorage.setItem("token", data.token);
                                 }
                        // this.props.history.push('/loginForm');
                        window.location.href = '/loginForm'
                       })
                      .then(() => this.setState({ redirect: true }))
                      .then(data => {
                        alert("Registration Success");
                    })
        }                          
        
    };  
               
    render(){
        // const { redirect } = this.state;
        //const {errors} = this.state;
        // if (redirect) {
        //     return <Redirect to='/loginForm'/>;
        // }
        return(
            <div style={{marginTop:'30px'}}>
                <Container>
             
             <Row>
                 <Col xs={12}><h3 className="text-center" style={{marginTop:'4%'}} >Registration Form </h3>
                 </Col>
             </Row>
      
             <Row style={{marginTop:'4%'}}>
                <Col lg="3" md="3" sm="12" xs="12">
                </Col>
              <Col lg="6" md="6" sm="12" xs="12">
            <Card>
                <CardBody style={{backgroundColor:'#fff'}}>
                    <FormGroup>
                        <p style={{textAlign:'left'}} >User Name</p>
                        <Input
                            type="text"
                            name="name"
                            placeholder="User Name"
                            onChange={(event) => this.setState({name: event.target.value})}                          
                            />
                    </FormGroup>
                    
                    <FormGroup>
                        <p style={{textAlign:'left'}} >User Email</p>
                        <Input
                            type="email"
                            name="email"
                            placeholder="User Email"
                            onChange={(event) => this.setState({email: event.target.value})}
                            />
                    </FormGroup>
                    
                    <FormGroup>
                        <p style={{textAlign:'left'}} > Password</p>
                        <Input
                            type="password"
                            name="Password"
                            placeholder="Password" 

                            onChange={(event) => this.setState({password: event.target.value})}
                            />
                        <Form.Text className="text-muted" style={{textAlign:'left'}}>
                                Password should contain atleast 8 characters
                        </Form.Text>
                    </FormGroup>
                 
                     
                    <FormGroup>
                        <p style={{textAlign:'left'}} >Phone Number</p>
                        <Input
                            type="text"
                            name="mobilenumber"
                            placeholder="Phone Number"
                            onChange={(event) => this.setState({phone: event.target.value})}
                            />
                    </FormGroup>
                 
                    <Button variant="primary" type="submit"  onClick = { this.handleSubmit  }  active>
                         Submit
                    </Button>
                </CardBody>
            </Card>
            </Col>
              
             </Row>
     
             </Container>
            </div>    

        );
    }
}