import React, { Component } from 'react'
import Cookies from 'js-cookie'
import Card from "react-bootstrap/Card";
import {Button,Row,Col} from 'reactstrap';
// import CardDeck from 'react-bootstrap/CardDeck'
import SweetAlert from 'react-bootstrap-sweetalert';
import Jumbotron from 'react-bootstrap/Jumbotron'


class Reset extends Component {
    constructor(props) {
        super(props);
        this.state={
            busData: [],
            data:[],
            alert: null,
            busName: '',
            busTiming: '',
            busFromTo: '',
            noOfTickets: ''
        }
        if (!Cookies.get('admin-token')){
            window.location.href='/admin'
            // this.props.history.push('/admin');
        }
        this.allBus()
        this.resetAll = this.resetAll.bind(this);
        this.getOpenTickets = this.getOpenTickets.bind(this)
        this.getClosedTickets = this.getClosedTickets.bind(this)
        this.viewUserDeatils = this.viewUserDeatils.bind(this)
        this.hideAlert = this.hideAlert.bind(this)
        this.addBus = this.addBus.bind(this)
        this.addBusDetails = this.addBusDetails.bind(this)
        this.onChange = this.onChange.bind(this);
        this.resetBus = this.resetBus.bind(this);
        this.allBus = this.allBus.bind(this);
        
    }

    async resetBus(busId){
        await fetch('http://localhost:5555/admin/reset/'+busId,{
            method: 'GET',
            headers: {
                'content-type': 'application/json',
                'x-access-token': Cookies.get('admin-token')
            },
        }).then(res=> res.json())
        .then(data=>{
            if(data.auth === true){
                const getAlert = () => (
                    <SweetAlert 
                      success 
                      title= "Message"
                      onConfirm={() => this.hideAlert()}
                    >
                      {data.msg}
                    </SweetAlert>
                  );
                  this.setState({
                    alert: getAlert()
                  });
            }
        }).then(async ()=> await this.allBus())
    }

    async deleteBus(busId){
        await fetch('http://localhost:5555/show/deletebus/'+busId,{
            method: 'DELETE',
            headers: {
                'content-type': 'application/json',
                'x-access-token': Cookies.get('admin-token')
            },
        }).then(res=> res.json())
        .then(data=>{
            if(data.auth === true){
                const getAlert = () => (
                    <SweetAlert 
                      success 
                      title= "Message"
                      onConfirm={() => this.hideAlert()}
                    >
                      {data.msg}
                    </SweetAlert>
                  );
                  this.setState({
                    alert: getAlert()
                  });
            }
        }).then(async ()=> await this.allBus())
    }

    async allBus(){
        
        await fetch('http://localhost:5555/show',{
            method: 'GET',
            headers:{
                'contene-type': "application/json",
                // "x-access-token": Cookies.get('token') 
            }
        }).then(response=> response.json())
        .then(dataObj => {
            if (dataObj.auth === true){
                const bus = dataObj.data.Buses
                this.setState({ 
                    busData : (bus)
                })
            }
        })
    }

    getOpenTickets(e) { 
        e.preventDefault();

        fetch ("http://localhost:5555/admin/opentickets ", {
            method: "GET",
            headers: {
              "content-Type": "application/json",
              "x-access-token":Cookies.get('admin-token')
            }
        }).then(response => 
            response.json())
            .then(jsonObj => {
                if (jsonObj.auth === true){
                    if (jsonObj.data){
                        this.setState({ 
                            data : (jsonObj.data.tickets),
                        })
                    }
                    else{
                        alert(jsonObj.msg);
                    }
                }
                else{
                    alert(jsonObj.message)
                }
            })
    }

    

    getClosedTickets(e){ 
        e.preventDefault();
        fetch ("http://localhost:5555/admin/closedtickets ", {
            method: "GET",
            headers: {
                'content-type':'application/json',
                "x-access-token":Cookies.get('admin-token')
            }
        }).then(response => 
            response.json())
            .then(jsonObj => {
                if (jsonObj.auth === true){
                    if (jsonObj.data){
                        this.setState({ 
                            data : (jsonObj.data.tickets),
                        })
                    }
                    else{
                        alert(jsonObj.msg);
                    }
                }
                else{
                    alert(jsonObj.message)
                }
            })
    }

    hideAlert() {
        console.log('Hiding alert...');
        this.setState({
          alert: null
        });
      }

    resetAll(e){
        e.preventDefault();
        fetch('http://localhost:5555/admin/resetall',{
            method: 'GET',
            headers:{
                'content-type': 'application/json',
                'x-access-token': Cookies.get('admin-token')
            }
        }).then(res=> res.json())
        .then(data=> {
            const getAlert = () => (
                <SweetAlert 
                  success 
                  title= "Message"
                  onConfirm={() => this.hideAlert()}
                >
                  {data.msg}
                </SweetAlert>
              );
              this.setState({
                alert: getAlert()
              });
            // alert(data.msg);
        })
    }
    async addBusDetails(){
        console.log(this.state);
        const busDetails = {
            name: this.state.busName,
            timing: this.state.busTiming,
            fromTo: this.state.busFromTo,
            noOfTickets: this.state.noOfTickets
        }
        await fetch('http://localhost:5555/show/add',{
            method: 'POST',
            headers:{
                'content-type': 'application/json',
                'x-access-token': Cookies.get('admin-token')
            },
            body: JSON.stringify(busDetails)
        }).then(res=> res.json())
        .then(data=> {
            const getAlert = () => (
                <SweetAlert 
                  success 
                  title= "Message"
                  onConfirm={() => this.hideAlert()}
                >
                  {data.msg}
                </SweetAlert>
              );
              this.setState({
                alert: getAlert()
              });
            // alert(data.msg);
        }).then(async ()=> await this.allBus())
    }
    onChange(e){
        this.setState({[e.target.name]:e.target.value})
    }

    addBus(){
        const getAlert = () => (
            <SweetAlert 
              success 
              title= "Add Bus"
              onConfirm={() => {this.addBusDetails();this.hideAlert()}}
            >
                <h4>Please Provide Following Details:</h4>
                Bus Name        :<input name="busName" id="busName" onChange={(event) => this.setState({busName: event.target.value})} placeholder='Enter Bus Name' class="form-control"/><hr />
                Bus Timing      :<input name="busTiming" id="busTiming" onChange={(event) => this.setState({busTiming: event.target.value})} placeholder='Enter Bus timing' class="form-control"/><hr />
                Bus FromTo      :<input name="busFromTo" id="busFromTo" onChange={(event) => this.setState({busFromTo: event.target.value})} placeholder='Enter Bus From to' class="form-control"/><hr />
                No. of tickets  :<input name="noOfTickets" id="noOfTickets" onChange={(event) => this.setState({noOfTickets: event.target.value})} placeholder='Enter No of Tickets' class="form-control"/><hr />
            </SweetAlert>
          );
          this.setState({
            alert: getAlert()
          });

    }
    

    viewUserDeatils(id){
        fetch('http://localhost:5555/booking/'+id,{
            method: 'GET',
            headers:{
                'content-type': 'application/json',
                'x-access-token': Cookies.get('admin-token')
            }
        }).then(res=> res.json())
        .then(object=> {
            if (object.auth===true){
                const data = object.data.users[0]
                const getAlert = () => (
                    <SweetAlert 
                      success 
                      title= "User Details"
                      onConfirm={() => this.hideAlert()}
                    >
                      User Name         : {data.userName} <hr/>
                      User Email        : {data.userEmail} <hr/>
                      User Phone Number : {data.phoneNumber} <hr/>
                    </SweetAlert>
                  );
              
                  this.setState({
                    alert: getAlert()
                  });
                // alert(object.data.users[0]);
            }
            else{
                alert(object.message)
            }
        })
    }

    render() {
        const details = this.state.data
        const renderCard = (dynamicData,Key)=>{
            if (dynamicData.status === 'CLOSED'){
                return (
                        <Col>
                            <Card style={{ width: '28rem',flex: 1}}>
                                <Card.Body>
                                <Card.Title>Ticket {Key+1}</Card.Title>
                                {/* <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle> */}
                                <Card.Text>
                                    <p>Ticket Id:{dynamicData.ticketId}</p>
                                    <p>Seat No: {dynamicData.seatNo}</p>
                                    <p>Status: {dynamicData.status}</p>
                                </Card.Text>
                                <Card.Link onClick={()=> this.viewUserDeatils(dynamicData.ticketId)}>View User Details</Card.Link>
                                {/* <Card.Link href="#">Another Link</Card.Link> */}
                                </Card.Body>
                            </Card>
                        </Col>
                
                    )
            }
            else{
                return (
                            <Col>
                                <Card style={{ width: '28rem',flex: 1}}>
                                    <Card.Body>
                                    <Card.Title>Ticket {Key+1}</Card.Title>
                                    {/* <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle> */}
                                    <Card.Text>
                                        <p>Ticket Id:{dynamicData.ticketId}</p>
                                        <p>Seat No: {dynamicData.seatNo}</p>
                                        <p>Status: {dynamicData.status}</p>
                                    </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                        )
            }
        }
        return (
            <div style={{marginTop:'30px'}}>
                <div class="container">
                    <h1 class="text-center text-black pt-5">Reset All booking and Tickets</h1>
                    <div class="form-group" style={{paddingLeft:'350px'}}>
                        {/* <input type="button" style={{width:"20%",margin:'20px'}} name="openbutton" class="btn btn-info btn-md" onClick = {this.getOpenTickets} value="Open Tickets"/> 
                        <input type="button" style={{width:"20%",margin:'20px'}} name="closebutton" class="btn btn-info btn-md" onClick = {this.getClosedTickets} value="Closed Tickets"/> */}
                        <input type="button" style={{width:"20%",margin:'20px'}} name="reset" class="btn btn-info btn-md" onClick={this.addBus} value="Add Bus"/>
                        <input type="button" style={{width:"20%",margin:'20px'}} name="reset" class="btn btn-info btn-md" onClick={this.resetAll} value="Reset All Bus"/>
                    </div>
                </div>
                <div class="container">
                {this.state.busData.map((dynamicData, key) => {
                          return (
                            <Jumbotron>
                            <h1>{dynamicData.busName}</h1>
                            <p>
                              Timing: {dynamicData.busTiming}
                              FromTo: {dynamicData.busFromTo}
                            </p>
                            <p>
                              <Button variant="primary" onClick={()=> this.resetBus(dynamicData.busId)}>Reset</Button>
                              <Button style={{marginLeft:'15px'}} onClick={()=> this.deleteBus(dynamicData.busId)} variant="primary">Delete</Button> 
                            </p>
                          </Jumbotron>
                          )
                    }
                    )}
                </div>
                <div  class="container">

                <Row md='2'>
                    {
                        details.map(renderCard)
                    }
                </Row>
                </div>
                {this.state.alert}
            </div>
        )
    }
}

export default Reset;