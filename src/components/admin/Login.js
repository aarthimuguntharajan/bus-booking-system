import React, { Component } from 'react'
import Cookies from 'js-cookie'
import './login.css'

class Login extends Component {
    constructor(props) {
        super(props);
        if (Cookies.get('admin-token')){
            window.location.href= '/reset';
            // this.props.history.push('/reset');
        }
        // handle initialization activities
        this.state = {
            adminEmail: '',
            password: ''
        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e){
        this.setState({[e.target.name]:e.target.value})
    }

    onSubmit(e){
        e.preventDefault();
        const data = {
            email: this.state.adminEmail,
            password: this.state.password
        }
        console.log('before',JSON.stringify(data));
        fetch('http://localhost:5555/admin/login',{
            method: 'POST',
            headers: {
                'content-type':'application/json'
            },
            body: JSON.stringify(data)
        }).then(res => res.json())
        .then(data => {
            if (data.auth === true){
                Cookies.set('admin-token',data.token);
            }
            // console.log('after',data);
            window.location.href= '/reset';
            // this.props.history.push('/reset');
        });

    }

    render() {
        return (
            <div id="login" style={{marginTop:'30px'}}>
                <h3 class="text-center text-red pt-5">Admin Login</h3>
                <div class="container">
                    <div id="login-row" class="row justify-content-center align-items-center">
                        <div id="login-column" class="col-md-6">
                            <div id="login-box" class="col-md-12">
                                <form id="login-form" class="form" onSubmit={this.onSubmit}>
                                    <h3 class="text-center text-info">Login</h3>
                                    <div class="form-group">
                                        <label for="adminEmail" class="text-info" >Username:</label><br/>
                                        <input type="text" name="adminEmail" value={this.state.adminEmail} onChange={this.onChange} id="adminEmail" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="text-info">Password:</label><br/>
                                        <input type="password" name="password" value={this.state.password} onChange={this.onChange} id="password" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        {/* <label for="remember-me" class="text-info"><span>Remember me</span> <span><input id="remember-me" name="remember-me" type="checkbox"></span></label><br> */}
                                        <input type="submit" name="submit" class="btn btn-info btn-md" value="submit"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Login;